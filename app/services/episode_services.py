from .helper import add_commit
from flask import current_app
from app.model.episode_model import EpisodeModel
from app.model.character_model import RickandMorty
from app.model.character_episode_model import CharacterEpisodeModel

def create_episode(data: dict) -> dict:
    episode = EpisodeModel(**data)

    add_commit(episode)

    return {
        "id": episode.id,
        "name": episode.name,
        "season": episode.season,
        "episode": episode.episode
    }

def select_episodes_characters(episode_id:int):

    session = current_app.db.session

    char_list: list[tuple[RickandMorty,EpisodeModel]] = (
        session.query(RickandMorty, EpisodeModel)
        .select_from(RickandMorty)
        .join(CharacterEpisodeModel)
        .join(EpisodeModel)
        .filter(EpisodeModel.id == episode_id)
        .all()
    )

    return [
        {"char_id": char.id, "char_name": char.name, "ep_id": ep.id, "ep_name": ep.name}
        for char, ep in char_list
    ]