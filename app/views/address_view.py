from ..services import add_commit
from flask import Blueprint, request
from sqlalchemy.exc import IntegrityError

from app.model.address_model import AddressModel

bp = Blueprint('address_route', __name__)

@bp.route("/address", methods=["POST"])
def create():
    data = request.get_json()

    new_address = AddressModel(**data)

    try:

        add_commit(new_address)

        return {
            'id' : new_address.character_id,
            'number': new_address.number,
            'coordenates' : new_address.coordenates,
            'character_id': new_address.character_id
        }

    except IntegrityError as e:
        return {"status": f"character_id {new_address.character_id} já está em uso"}


    
