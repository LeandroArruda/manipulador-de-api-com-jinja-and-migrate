from http import HTTPStatus

class StatusOptionError(Exception):
    field_options = ["alive","dead","unknown"]

    def __init__(self, data: dict) -> None:
        self.message = (
            {
                "error": {
                    "status_options" : self.field_options,
                    "received_option" : data["status"]
                }
            }, HTTPStatus.BAD_REQUEST,
        )
        super().__init__(self.message)