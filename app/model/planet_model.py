from sqlalchemy.orm import backref, relationship
from sqlalchemy.sql.sqltypes import Boolean
from . import db
from sqlalchemy import Column, Integer, String

class PlanetModel(db.Model):
    __tablename__ = 'planets'

    id = Column(Integer, primary_key=True)

    name = Column(String(150), nullable=False)
    population = Column(Integer, nullable=False)
    is_extinct = Column(Boolean, default=False)

    address_list = relationship("AddressModel", backref=backref('planet'))