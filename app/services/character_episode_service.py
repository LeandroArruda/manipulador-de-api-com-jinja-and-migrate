from .helper import add_commit
from app.model.episode_model import EpisodeModel
from app.model.character_model import RickandMorty
from app.model.character_episode_model import CharacterEpisodeModel

def create_character_episode(data: dict) -> dict:
    episode: EpisodeModel = EpisodeModel.query.get(data["episode_id"])
    character: RickandMorty = RickandMorty.query.get(data["character_id"])

    new_char_ep = CharacterEpisodeModel(character_id=character.id, episode_id=episode.id)

    add_commit(new_char_ep)

    return {
        'id': new_char_ep.id,
        'character_id': character.id,
        'episode_id' : episode.id
    }