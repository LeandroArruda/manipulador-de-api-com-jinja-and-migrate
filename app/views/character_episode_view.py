from flask import Blueprint, request
from http import HTTPStatus
from app.services.character_episode_service import create_character_episode

bp = Blueprint("bp_char_ep_route",__name__)

@bp.route("/character_episode", methods=["POST"])
def create():
    data = request.get_json()

    return create_character_episode(data), HTTPStatus.CREATED



