from flask import Blueprint, request, jsonify
from http import HTTPStatus
from app.services.episode_services import create_episode, select_episodes_characters

bp = Blueprint("bp_episode_route", __name__)

@bp.route("/episodes", methods=["POST"])
def create():
    data = request.get_json()
    
    return create_episode(data), HTTPStatus.CREATED

@bp.route("/episodes/<int:episode_id>/characters")
def retrieve_episodes_characters(episode_id: int):

    return jsonify(select_episodes_characters(episode_id)), HTTPStatus.OK