from ..config.database import db
from .planet_model import PlanetModel
from .character_model import RickandMorty
from .address_model import AddressModel
from .character_episode_model import CharacterEpisodeModel
from .episode_model import EpisodeModel