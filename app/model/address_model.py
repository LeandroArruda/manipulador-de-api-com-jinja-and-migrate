from . import db
from sqlalchemy.orm import backref, relationship
from sqlalchemy import Column, Integer, String, ForeignKey


class AddressModel(db.Model):
    __tablename__ = "addresses"

    id = Column(Integer, primary_key=True)

    street = Column(String(150), nullable=False)
    number = Column(Integer)
    coordenates = Column(String(50), nullable=False)

    character_id = Column(Integer, ForeignKey("characters.id"), nullable=False, unique=True)

    planet_id = Column(Integer, ForeignKey('planets.id'), nullable=False)

    character = relationship("RickandMorty", backref=backref("address"), uselist=False)
    
