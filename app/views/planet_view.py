from app.model.address_model import AddressModel
from flask import request, Blueprint, jsonify, current_app

from ..services import add_commit
from app.model.planet_model import PlanetModel

bp = Blueprint("planet_route", __name__)

@bp.route("/planet", methods=["POST"])
def planet():
    data = request.get_json()

    new_planet = PlanetModel(**data)

    add_commit(new_planet)

    return {
        'id':new_planet.id,
        'name':new_planet.name,
        'population':new_planet.population
        }

@bp.route("/planet/<int:planet_id>/address")
def get_planet(planet_id:int):

    session = current_app.db.session

    address_list : list[AddressModel] = (
        session.query(AddressModel)
        .join(PlanetModel, PlanetModel.id == AddressModel.planet_id )
        .filter(PlanetModel.id == planet_id)
        .all()
    )

    return jsonify([{
        "id": address.id,
        "street" : address.street,
        "character": address.character.name,
        "planet": address.planet.name
    } for address in address_list])
