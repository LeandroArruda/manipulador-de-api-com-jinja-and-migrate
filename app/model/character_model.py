from . import db
from sqlalchemy import Column, Integer, String

class RickandMorty(db.Model):

    __tablename__ = "characters"
    
    id = Column(Integer, primary_key=True)

    name = Column(String(150), nullable=False)
    gender = Column(String(150), nullable=False)
    status = Column(String(150), nullable=False)
    img_url = Column(String, nullable=False, unique=True)

    def __repr__(self) -> str:
        return f'<{self.name} - {self.id}>'