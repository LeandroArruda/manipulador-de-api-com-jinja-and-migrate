from flask import Blueprint, request, render_template, redirect, jsonify, current_app
from app.services.characters_services import create_character,add_commit
from app.model.character_model import RickandMorty
from ..exc import StatusOptionError, GenderOptionError, MissingKeyError, RequiredKeyError
from app import db
from http import HTTPStatus

bp = Blueprint('bp_character_route',__name__)


@bp.route("/character", methods=["GET"])
def retrieve():
    characters = RickandMorty.query.all()

    return render_template("character.html", characters=characters)

@bp.route("/")
def home():
    return render_template("home.html")

@bp.route("/character/search", methods=["POST"])
def create_search():
    search = request.form.get("search")

    if search:
        return redirect(f"/character/search?name={search}")

    return redirect("/character")

@bp.route("/character/search", methods=["GET"])
def retrieve_search():

    name_search = request.args.get("name")

    search_list = RickandMorty.query.filter(RickandMorty.name.ilike(f'%{name_search}%')).all()

    return render_template("character.html", characters=search_list)

@bp.route("/characters", methods=["POST"])
def create():

    data = request.get_json()
    
    try:
        return jsonify(create_character(data)), HTTPStatus.CREATED
    
    except MissingKeyError as e:
        return e.message

    except RequiredKeyError as e:
        return e.message


@bp.route("/characters/<int:char_id>", methods=["DELETE"])
def delete(char_id: int):
    found_char = RickandMorty.query.get(char_id)

    session = current_app.db.session

    if found_char:
        session.delete(found_char)
        session.commit()

        return "",HTTPStatus.NO_CONTENT
    
    else:
        return {"status": "character not found"}, HTTPStatus.NOT_FOUND


@bp.route("/characters/<int:char_id>", methods=["PATCH"])
def update(char_id: int):

    data = request.get_json()

    found_char : RickandMorty = RickandMorty.query.get(char_id)

    if not found_char:
        return {"status": "character not found"}, HTTPStatus.NOT_FOUND

    for key, value in data.items():
        setattr(found_char,key,value)

    add_commit(found_char)

    return {
        'id': found_char.id,
        'name': found_char.name,
        'gender': found_char.gender,
        'status': found_char.status,
        'img_url': found_char.img_url
    }, HTTPStatus.OK
