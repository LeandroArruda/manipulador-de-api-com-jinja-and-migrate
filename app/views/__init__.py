from flask import Flask

def init_app(app: Flask):
    from .character_view import bp as bp_character
    from .address_view import bp as bp_address
    from .planet_view import bp as bp_planet
    from .character_episode_view import bp as bp_char_ep
    from .episode_view import bp as bp_episode

    app.register_blueprint(bp_character)
    app.register_blueprint(bp_address)
    app.register_blueprint(bp_planet)
    app.register_blueprint(bp_episode)
    app.register_blueprint(bp_char_ep)