from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from environs import Env
from app import views
from flask_migrate import Migrate

from app.config import commands
from app.config import database
from app.config import migrations


env = Env()
env.read_env()

db = SQLAlchemy()

mg = Migrate()

def create_app():

    app=Flask(__name__)

    app.config["SQLALCHEMY_DATABASE_URI"] = env("SQLALCHEMY_DATABASE_URI")
    app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
    app.config["JSON_SORT_KEYS"] = False

    database.init_app(app)
    migrations.init_app(app)
    commands.init_app(app)
    views.init_app(app)

    return app



