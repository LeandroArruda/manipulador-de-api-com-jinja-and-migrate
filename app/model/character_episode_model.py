from . import db
from sqlalchemy import Integer, Column, ForeignKey

class CharacterEpisodeModel(db.Model):
    __tablename__ = "character_episodes"

    id = Column(Integer, primary_key=True)

    character_id = Column(Integer,ForeignKey("characters.id"))
    episode_id = Column(Integer,ForeignKey("episodes.id"))
    