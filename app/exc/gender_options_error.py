from http import HTTPStatus

class GenderOptionError(Exception):
    field_options = ["female","male","genderless","unknown"]

    def __init__(self, data: dict) -> None:
        self.message = (
            {
                "error": {
                    "gender_options" : self.field_options,
                    "received_option" : data["gender"]
                }
            }, HTTPStatus.BAD_REQUEST,
        )
        super().__init__(self.message)
