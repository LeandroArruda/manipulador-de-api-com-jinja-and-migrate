from . import db
from sqlalchemy import Column, Integer, String
from sqlalchemy.orm import backref, relationship


class EpisodeModel(db.Model):
    __tablename__ = "episodes"

    id = Column(Integer, primary_key=True)

    name = Column(String(150), nullable=False)
    season = Column(Integer, nullable=False)
    episode = Column(Integer, nullable=False)

    characters_list = relationship(
        "RickandMorty", 
        backref=backref("episodes_list"),
        secondary="character_episodes",
    )
