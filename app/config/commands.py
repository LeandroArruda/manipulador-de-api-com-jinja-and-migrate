from flask import Flask
from flask.cli import AppGroup
from ..model import RickandMorty
from ..services import add_all_commit
import requests
import click

def cli_char(app: Flask):
    cli_group_char = AppGroup("characters")

    @cli_group_char.command("populate")
    @click.argument("gender")
    @click.argument("status")
    def cli_char_create(gender:str, status:str):
        api_response = requests.get(
        f"https://rickandmortyapi.com/api/character/?gender={gender}&status={status}"
        )

        results = api_response.json()["results"]

        char_list = [
            {
                key if key != 'image' else 'img_url': value 
                for key, value in char.items()
                if key in ['name','gender','status','image'] 
            }
            for char in results
        ]

        model_list: list[RickandMorty] = [RickandMorty(**char) for char in char_list]

        for char in model_list:
            click.echo(f"Character {char.name} created!")

        add_all_commit(model_list)


        return char_list

    app.cli.add_command(cli_group_char)

def init_app(app: Flask):
    cli_char(app)
