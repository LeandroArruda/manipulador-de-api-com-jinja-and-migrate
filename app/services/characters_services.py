from app.services.helper import add_commit, verify_missing_key, verify_recieved_keys
from app.exc.missing_key import MissingKeyError
from app.exc.required_key import RequiredKeyError
from app.model import RickandMorty


def create_character(data:dict):

    key_list = ["name","gender","status","img_url"]

    if verify_missing_key(data,key_list):
        raise MissingKeyError(data,key_list)

    if verify_recieved_keys(data,key_list):
        raise RequiredKeyError(data,key_list)

    new_character = RickandMorty(**data)
    
    add_commit(new_character)

    return {
        'id': new_character.id,
        'gender': new_character.gender,
        'status': new_character.status,
        'img_url': new_character.img_url
    }